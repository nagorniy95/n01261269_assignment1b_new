﻿<%@ Page Language="C#" Inherits="n01261269.hotel" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>hotel</title>
</head>
<body>
       <form id="form1" runat="server">
           <div>
                <p runat="server" id="information"></p>
                <h2>Hotel Booking Reservation</h2>
                <p runat="server" id="test"></p>
           </div>
            <!-- ========First Name======== -->
            <label>First Name:</label><br />
            <asp:TextBox runat="server" ID="clientFirstName" placeholder="Your First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a  First Name" ControlToValidate="clientFirstName" 
                                        ID="validatorFirstName"></asp:RequiredFieldValidator> 
            <br />
            
            <!-- =======Last Name======== -->
            <label>Last Name:</label><br />
            <asp:TextBox runat="server" ID="clientLastName" placeholder="Your Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Last Name" ControlToValidate="clientLastName" 
                                                ID="validatorLastName"></asp:RequiredFieldValidator>
            <br />
            
            <!-- ===========Pnone========== -->
            <label>Phone Number:</label><br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Your Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a valid Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <!--It can compare two values, for instance the values of two controls. --> 
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="6479157656" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            
            <!-- =========Email========= -->
            <label> Email:</label><br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
                    
            <!-- ==========Check-in Time========= -->
            <label>Check-in time:</label><br />        
            <asp:TextBox runat="server" ID="checkintime" placeholder="Check-in time (24 hour format)"></asp:TextBox>
            <asp:RangeValidator EnableClientScript="false" ID="hourscontrol" runat="server" ControlToValidate="checkintime" MinimumValue="1100" Type="Integer" MaximumValue="2300" ErrorMessage="Our hours are between 11am and 11pm"></asp:RangeValidator>
            <br />
                 
            <!-- ========Amount of days===========-->
            <label>Amount of days:</label><br/>
            <asp:TextBox runat="server" ID="bookingDays" placeholder="Amount of days (1-30)"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="bookingDays" Type="Integer" MinimumValue="1" MaximumValue="30" ErrorMessage="Enter a valid number of days (between 1 and 30)"></asp:RangeValidator>
            <br />
                  
            <!-- ========Room Size========= -->
            <label>Room size</label>
                <asp:DropDownList runat="server" ID="RoomSize">
                    <asp:ListItem Value="S" Text="20 m"></asp:ListItem>
                    <asp:ListItem Value="M" Text="30 m" Selected></asp:ListItem>
                    <asp:ListItem Value="L" Text="40 m"></asp:ListItem>
                </asp:DropDownList>
            <br />
            
           <!-- =========Smokers Room========== -->      
           <label>Smoke/No"</label>
           <asp:RadioButtonList runat="server" id="roomSmoking">
                <asp:ListItem runat="server" Text="For Smokers" GroupName="smoke"/>
                <asp:ListItem runat="server" Text="Not for Smokers" GroupName="smoke"/>
            </asp:RadioButtonList>
            <br />
            
            <!-- =========Bed Type============== -->
            
            <label>Bed type</label><br/>
            <asp:RadioButtonList runat="server" id="roomBedType">
                <asp:ListItem runat="server" Text="Single" GroupName="bed" />
                <asp:ListItem runat="server" Text="Separate" GroupName="bed" />
            </asp:RadioButtonList>
            <br />
            
                 
            <!-- =======Main Preferences======== -->
            <label>Main Preferences</label>
            <div ID="preferences" runat="server">
                <asp:CheckBox runat="server" ID="miniBar" Text="Mini bar" />
                <asp:CheckBox runat="server" ID="balcony" Text="Balcony" />
                <asp:CheckBox runat="server" ID="bathroom" Text="Bathroom" />
        
            </div>
                   
            <!-- =====Meal===== -->
            <label>Meal:</label><br />
            <div id="meal_container" runat="server">
                <asp:CheckBox runat="server" ID="breakfast" Text="Breakfast" />
                <asp:CheckBox runat="server" ID="lunch" Text="Lunch" />
                <asp:CheckBox runat="server" ID="dinner" Text="Dinner" />
            </div>
            
            <!-- =======Message======== -->
            <asp:TextBox id="TextArea1" TextMode="multiline" Columns="50" Rows="5" runat="server" placeholder="Your message here..." Width="30%" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="TextArea1" ErrorMessage="Please enter your message"></asp:RequiredFieldValidator>
            <br />
            
            <!-- ======Submit button====== -->
            <asp:Button runat="server" ID="myButton" OnClick="Book" Text="Submit"/>
            <br />
                
            <!-- Validation Summary -->    
            <div>
            <asp:ValidationSummary id="valSum" DisplayMode="BulletList" EnableClientScript="true" HeaderText="You must enter a value in the following fields:"
                          runat="server"/> 
            </div>
            
            <!-- =======Results======== -->
            <div runat="server" ID="res"></div>   
            <div runat="server" id="OrderRes"></div>
            

            <footer runat="server" id="footer"></footer>
    </form>
</body>
</html>