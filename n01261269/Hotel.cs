﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace n01261269
{
    public class Hotel
    {
        //information to store in my Hotel object 

        //Meal
        //RoomSize
        //days

        public List<string> mymeal;

        public string myroomsize;

        public int days;

        public int time;

        public string textarea;

        public List<string> mpreferences;

        public string smoke;

        public string roomBad;

        //constractor function 
        public Hotel(List<string> m, string r, int d, int t, string text, List<string> mp, string s, string room)
        {
            mymeal = m;
            myroomsize = r;
            days = d;
            time = t;
            textarea = text;
            mpreferences = mp;
            smoke = s;
            roomBad = room;
        }

    }
}
