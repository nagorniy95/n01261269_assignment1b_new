﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace n01261269
{

    public partial class hotel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)

        {
            test.InnerHtml = " My Form:";

        }


        protected void Book(object sender, EventArgs e)
        {
            // When Press submit button it shoud display information
            // which was behind 
            if (!Page.IsValid)
            {
                return;
            }
            information.InnerHtml = "Thanks for choice!";


            //Hotel object
            string myroomsize = RoomSize.SelectedItem.Value.ToString();
            int days = int.Parse(bookingDays.Text);
            int time = int.Parse(checkintime.Text);
            string textarea = TextArea1.Text.ToString();
            string smokeorno = roomSmoking.SelectedItem.Text;
            string roombad = roomBedType.SelectedItem.Text;

            List<String> mpref = new List<String>();
            // for my main preferences
            foreach (Control control in preferences.Controls)
                {
                    if (control.GetType() == typeof(CheckBox))
                    {
                        CheckBox mpreferences = (CheckBox)control;
                        if (mpreferences.Checked)
                        {
                            mpref.Add(mpreferences.Text);
                        }

                    }
                }

            List<String> meal = new List<String>();
            // for the meal type
            foreach (Control control in meal_container.Controls)
                {
                    if(control.GetType() == typeof(CheckBox))
                    {
                        CheckBox mymeal = (CheckBox)control;
                        if (mymeal.Checked)
                        {
                            meal.Add(mymeal.Text);
                        }

                    }
                }

            Hotel newhotel = new Hotel(meal, myroomsize, days, time, textarea, mpref, smokeorno, roombad);



            //Creating the clients object
            string fname = clientFirstName.Text.ToString();
            string lname = clientLastName.Text.ToString();
            string email = clientEmail.Text.ToString();
            string phone = clientPhone.Text.ToString();
            Customer newcustomer = new Customer();
            newcustomer.CustomerFirstName = fname;
            newcustomer.CustomerLastName = lname;
            newcustomer.CustomerPhone = phone;
            newcustomer.CustomerEmail = email;


            Order neworder = new Order(newhotel, newcustomer);

            OrderRes.InnerHtml = neworder.PrintReceipt();

        }
    }
}
