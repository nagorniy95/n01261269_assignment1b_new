﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace n01261269
{


    public partial class hotel
    {

        protected System.Web.UI.HtmlControls.HtmlForm form1;

        protected System.Web.UI.HtmlControls.HtmlGenericControl test;
        protected System.Web.UI.HtmlControls.HtmlGenericControl information;
        protected global::System.Web.UI.WebControls.DropDownList RoomSize;
        protected global::System.Web.UI.WebControls.TextBox bookingDays;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl meal_container;
        protected global::System.Web.UI.WebControls.TextBox TextArea1;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl preferences;
        protected global::System.Web.UI.WebControls.RadioButtonList roomSmoking;
        protected global::System.Web.UI.WebControls.RadioButtonList roomBedType;


        protected global::System.Web.UI.WebControls.TextBox clientFirstName;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator validatorFirstName;
        protected global::System.Web.UI.WebControls.TextBox clientLastName;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator validatorLastName;
        protected global::System.Web.UI.WebControls.TextBox clientPhone;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator validatorPhone;
        protected global::System.Web.UI.WebControls.TextBox clientEmail;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regexEmailValid;
        protected global::System.Web.UI.WebControls.TextBox checkintime;
        protected global::System.Web.UI.WebControls.RangeValidator hourscontrol;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl OrderRes;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl footer;

        protected global::System.Web.UI.WebControls.Button myButton;
    }
}