﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace n01261269
{
    public class Customer
    {
        //Information in the customer that we want to store in the object
        //customerfname, 
        //customerlname,
        //customerphone,
        //customeremail

        private string customerFirstName;
        private string customerLastName;
        private string customerPhone;
        private string customerEmail;

        public Customer()
        {

        }

        public string CustomerFirstName
        {
            get { return customerFirstName; }
            set { customerFirstName = value; }
        }
        public string CustomerLastName
        {
            get { return customerLastName; }
            set { customerLastName = value; }
        }
        public string CustomerPhone
        {
            get { return customerPhone; }
            set { customerPhone = value; }
        }
        public string CustomerEmail
        {
            get { return customerEmail; }
            set { customerEmail = value; }
        }

    }
}
