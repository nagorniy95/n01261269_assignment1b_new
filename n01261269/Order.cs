﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace n01261269
{
    public class Order
    {
        // an order consists of the following pieces of information
        // customer
        // pizza
        // delivery information

        public Hotel hotel;
        public Customer customer;

        public Order(Hotel h, Customer c)
        {
            hotel = h;
            customer = c;

        }

        public string PrintReceipt()
        {
            string receipt = "Order Receipt:<br>";
            receipt += "Your total is :" + " $" + CalculateOrder().ToString() + "<br/>";
            receipt += "First Name: " + customer.CustomerFirstName + "<br/>";
            receipt += "Last Name: " + customer.CustomerLastName + "<br/>";
            receipt += "Phone Number: " + customer.CustomerPhone + "<br/>";
            receipt += "Email: " + customer.CustomerEmail + "<br/>";
            receipt += "Check-in time: " + hotel.time + "<br />";
            receipt += "Amount of days: " + hotel.days + "<br />";
            receipt += "Room size: " + hotel.myroomsize + "<br />";
            receipt += "Smoke option: " + hotel.smoke + "<br />";
            receipt += "Bad type: " + hotel.roomBad + "<br />";
            receipt += "Main Preferences: " + String.Join(", ", hotel.mpreferences.ToArray()) + "<br/>";
            receipt += "Meal type: " + String.Join(", ", hotel.mymeal.ToArray()) + "<br/>";
            receipt += "Your message: " + hotel.textarea + "<br />";

            return receipt;
        }
            public double CalculateOrder()
            {
                double total = 0;
                if (hotel.myroomsize == "S")
                {
                    total = 50;
                }
                else if (hotel.myroomsize == "M")
                {
                    total = 100;
                }
                else if (hotel.myroomsize == "L")
                {
                    total = 150;
                }

             total = total * hotel.days;

            return total;
            }
        }
    }


